Cracked Crystal C# Style Guide

This style guide was created to help us maintain code.

This style guide is based on **Unity** and **C#**

### Namespaces, Classes, Methods

All Namespaces, Classes and Methods are to be written in **PascalCase**, exceptions include words as GUI and HUD.

**EXAMPLE**
```csharp
void thisIsATest
```

### Variables

All Variables are to be written in **camelCase**, this includes public and static variables aswell.

**EXAMPLE**
```csharp
public class TestClass
{
    public string userInput;
    int userAge;
}
```

### Parameters

All Parameters are to be written in **camelCase**.

**EXAMPLE**
```csharp
void testFunc(int userAge)
```

### Indentation

Indentation should be done using **spaces**, set your program so a tab inserts 4 spaces.

**EXAMPLE**
```csharp
if(test)
{
    doCalc(test, test+100)
}
```

### Brace Style

All Braces are to be put on their own line as per C# convention, that includes else if chains.

**EXAMPLE**
```csharp
class TestClass
{
    void TestFunc(int test)
    {
        if(test)
        {
            
        }
    }
}
```

### Credits

This style guide was written by Andreas Zacchi in cooperation with Oliver Nordby Hansen.
